#pragma once

#include <type_traits>
#include <functional>


namespace maze_solver
{


// TYPE LIST UTIL

template<size_t T_INITIAL, class... T_Args>
struct ArgCount
{
  static constexpr size_t size = T_INITIAL + sizeof...(T_Args);
};

template<size_t T_INDEX, size_t T_COUNT, class T_Type, class... T_Types>
struct TypeList : ArgCount<1, T_Types...>
{
  using Type = std::conditional_t<T_INDEX == T_COUNT, T_Type, typename TypeList<T_INDEX, T_COUNT + 1, T_Types...>::Type>;
};

template<size_t T_INDEX, size_t T_COUNT, class T_Type>
struct TypeList<T_INDEX, T_COUNT, T_Type> : ArgCount<1>
{
  using Type = std::conditional_t<T_INDEX == T_COUNT, T_Type, void>;
};


// ENUMERATIONS

enum class Tile : int
{
  PATH = 0,
  WALL = 1,
  EXIT = 2
};

enum class Dir : int
{
  DOWN  = 0,
  LEFT  = DOWN + 1,
  UP    = LEFT + 1,
  RIGHT = UP   + 1
};

static constexpr const char* getDirStr(Dir dir)
{
#if __cplusplus <= 201103L //C++11
  return
    dir == Dir::DOWN  ? "DOWN " :
    dir == Dir::LEFT  ? "LEFT " :
    dir == Dir::UP    ? "UP   " :
    dir == Dir::RIGHT ? "RIGHT" : "INVALID";
#else //C++14
  switch (dir)
  {
  case Dir::DOWN:   return "DOWN ";
  case Dir::LEFT:   return "LEFT ";
  case Dir::UP:     return "UP   ";
  case Dir::RIGHT:  return "RIGHT";
  default:          return "INVALID";
  }
#endif
}

enum class RotDir
{
  CW,
  CCW
};

enum class EngineState
{
  IDLE,
  FIND_WALL,
  FOLLOW_WALL,
  EXIT_FOUND
};

static constexpr const char* getEngineStateStr(EngineState state)
{
#if __cplusplus <= 201103L //C++11
  return 
    state == EngineState::IDLE      ? "IDLE" :
    state == EngineState::FIND_WALL   ? "FIND_WALL" :
    state == EngineState::FOLLOW_WALL ? "FOLLOW_WALL" :
    state == EngineState::EXIT_FOUND  ? "EXIT_FOUND" : "INVALID";
#else //C++14
  switch (state)
  {
  case EngineState::IDLE:         return "IDLE";
  case EngineState::FIND_WALL:    return "FIND_WALL";
  case EngineState::FOLLOW_WALL:  return "FOLLOW_WALL";
  case EngineState::EXIT_FOUND:   return "EXIT_FOUND";
  default:                        return "INVALID";
  }
#endif
}


// MAZE BUILDER

/**
 * This struct holds the type of a tile or a cell in the maze.
 */
template<int T_TILE>
struct TilePack
{
  static constexpr Tile value = Tile(T_TILE);
};

/**
 * This struct contains the tiles in a row of the maze.
 */
template<int T_TILE, int... T_TILES>
struct Row : ArgCount<1, TilePack<T_TILES>...>
{
  template<size_t T_INDEX>
  static constexpr Tile get()
  {
    return TypeList<T_INDEX, 0, TilePack<T_TILE>, TilePack<T_TILES>...>::Type::value;
  }
};

/**
 * This struct represents the maze as an NxN matrix containing all the rows of tiles.
 */
template<class T_Row, class... T_Rows>
struct Maze : ArgCount<1, T_Rows...>
{
  template<size_t T_X, size_t T_Y>
  static constexpr Tile get()
  {
    return TypeList<T_Y, 0, T_Row, T_Rows...>::Type::template get<T_X>();
  }

  template<size_t T_INDEX>
  static constexpr size_t getSizeOfRow()
  {
    return TypeList<T_INDEX, 0, T_Row, T_Rows...>::Type::size;
  }
};


// POSITION AND FACING

template<int T_X = -1, int T_Y = -1>
struct Position
{
  static constexpr int x = T_X;
  static constexpr int y = T_Y;
  static constexpr bool isValid = x >= 0 && y >= 0;
};

template<class T_CurrPos = Position<>, Dir T_DIR = Dir::DOWN>
struct DirPos
{
  using Pos = T_CurrPos;
  static constexpr Dir dir = T_DIR;
};

template<class T_Pos1, class T_Pos2>
struct IsSamePos;

/**
 * This struct is used to check if two Position objects point to the same coordinate.
 */
template<int T_X1, int T_Y1, int T_X2, int T_Y2>
struct IsSamePos<Position<T_X1, T_Y1>, Position<T_X2, T_Y2>>
{
  static constexpr bool value = Position<T_X1, T_Y1>::x == Position<T_X2, T_Y2>::x &&
                                Position<T_X1, T_Y1>::y == Position<T_X2, T_Y2>::y;
};

/**
 * This struct is used to check if two DirPos objects point to the same coordinate
 * having the same direction (up, down, left or right).
 */
template<class T_Pos1, Dir T_DIR1, class T_Pos2, Dir T_DIR2>
struct IsSamePos<DirPos<T_Pos1, T_DIR1>, DirPos<T_Pos2, T_DIR2>>
{
  static constexpr bool value = IsSamePos<typename DirPos<T_Pos1, T_DIR1>::Pos, typename DirPos<T_Pos2, T_DIR2>::Pos>::value &&
                                DirPos<T_Pos1, T_DIR1>::dir == DirPos<T_Pos2, T_DIR2>::dir;
};


// STEP RECORDING

/**
 * This struct stores the current state of the engine for replaying the executed steps later.
 */
template<class T_CurrDirPos, class T_DirPosToEdge, EngineState T_STATE>
struct Record
{
  using DirPos = T_CurrDirPos;
  using DirPosToEdge = T_DirPosToEdge;
  static constexpr EngineState state = T_STATE;
};

/**
 * This struct is used to replay the steps taken to find the exit.
 */
template<class... T_Records>
struct RecordList;

template<class... T_Records>
struct RecordListBase
{
  template<class T_Record>
  using Append = RecordList<T_Records..., T_Record>;

  static constexpr size_t size = sizeof...(T_Records);
};

template<class... T_Records>
struct RecordList : RecordListBase<T_Records...>
{
  template<size_t INDEX>
  using GetRecord = typename TypeList<INDEX, 0, T_Records...>::Type;
};

template<>
struct RecordList<> : RecordListBase<>
{
};


// CORE UTILS

/**
 * This struct is used to transform the input DirPos instance into a new DirPos instance
 * that contains the same position, but having the original direction rotated by the specified
 * rotational direction (clockwise, counter-clockwise) in 90 degrees.
 */
template<class T_DirPos, RotDir T_ROT_DIR>
struct Rotate;

template<int T_X, int T_Y, Dir T_DIR, RotDir T_ROT_DIR>
struct Rotate<DirPos<Position<T_X, T_Y>, T_DIR>, T_ROT_DIR>
{
  using DirPos = maze_solver::DirPos<Position<T_X, T_Y>, Dir((4 + (int)T_DIR + (T_ROT_DIR == RotDir::CW ? 1 : -1)) % 4)>;
};

/**
 * This struct outputs a new DirPos instance that contains the coordinate of the next cell
 * from the input DirPos position in the direction of the input DirPos instance.
 */
template<class T_CurrDirPos>
struct GoAhead
{
  using DirPos = maze_solver::DirPos<Position<
      (T_CurrDirPos::Pos::x + ((int)T_CurrDirPos::dir % 2) * ((int)T_CurrDirPos::dir - 2)),
      (T_CurrDirPos::Pos::y - (((int)T_CurrDirPos::dir + 1) % 2) * ((int)T_CurrDirPos::dir - 1))>, T_CurrDirPos::dir>;
};

/**
 * Yields true if the input position is the same as the position of the exit cell in the maze.
 */
template<class T_Maze, class T_CurrPos>
struct IsExitFound
{
  static constexpr bool value = T_Maze::template get<T_CurrPos::x, T_CurrPos::y>() == Tile::EXIT;
};

/**
 * Yields true if the position of the next cell in the current direction is a wall.
 */
template<class T_Maze, class T_CurrDirPos>
struct IsWallAhead
{
  static constexpr bool value = T_Maze::template get<
      GoAhead<T_CurrDirPos>::DirPos::Pos::x, GoAhead<T_CurrDirPos>::DirPos::Pos::y>() == Tile::WALL;
};

/**
 * Outputs the a DirPos instance with the position of the next cell in the direction
 * of the input DirPos instance and turns left (90 degrees counter-clockwise) if there's
 * no wall in the new position, otherwise it outputs a DirPos instance with the same
 * position as the input position and turns right (90 degrees clockwise).
 */
template<class T_Maze, class T_CurrDirPos>
struct StepOne
{
  using DirPos = std::conditional_t<IsWallAhead<T_Maze, T_CurrDirPos>::value,
      typename Rotate<T_CurrDirPos, RotDir::CW>::DirPos,
      typename Rotate<typename GoAhead<T_CurrDirPos>::DirPos, RotDir::CCW>::DirPos>;
};

/**
 * This struct takes two DirPos instances and gives back the one which is closer
 * to the edge of the maze in the specified direction.
 */
template<class T_SavedDirPos, class T_DirPos, Dir T_INIT_DIR>
struct GetClosestDirPosToEdge
{
  using DirPos = std::conditional_t<!T_SavedDirPos::Pos::isValid ||
      ((int)T_INIT_DIR % 2 == 0 && (T_SavedDirPos::Pos::y - T_DirPos::Pos::y) * ((int)T_INIT_DIR - 1) > 0) ||
      ((int)T_INIT_DIR % 2 == 1 && (T_SavedDirPos::Pos::x - T_DirPos::Pos::x) * ((int)T_INIT_DIR - 2) < 0), T_DirPos, T_SavedDirPos>;
};


// CORE LOGIC

/**
 * This template struct tries to find the exit in the specified maze structure by recursively instantiating
 * itself and executing the proper steps in each instantiation. When the exit is found, the loop of
 * instantiation is broken by a template specialization.
 *
 *   T_CurrDirPos   - Holds the current position and direction.
 *   T_INIT_DIR     - Specifies the direction in which the engine seeks the edge wall of the maze.
 *   T_DirPosToEdge - Holds the closest position that has been touched during seeking the exit.
 *   T_RecList      - Contains the sequence of engine states that the engine went through during the seeking.
 *   T_STEP         - The current step which is increased by one when a new set of related actions are performed.
 *   T_STATE        - The current engine state.
 */
template<class T_Maze, class T_CurrDirPos, Dir T_INIT_DIR = T_CurrDirPos::dir, class T_DirPosToEdge = DirPos<>,
         class T_RecList = RecordList<>, int T_STEP = 0, EngineState T_STATE = EngineState::IDLE>
struct Engine;

/**
 * This struct is used to break the instantiation loop of the Engine template struct in case the input
 * condition is false.
 */
template<bool T_CONDITION, class T_Maze, class T_CurrDirPos, Dir T_INIT_DIR, class T_DirPosToEdge, class T_RecList, int T_STEP, EngineState T_STATE>
struct EngineEnableIf
{
  using Type = typename Engine<T_Maze, T_CurrDirPos, T_INIT_DIR, T_DirPosToEdge, T_RecList, T_STEP, T_STATE>::Type;
};

template<class T_Maze, class T_CurrDirPos, Dir T_INIT_DIR, class T_DirPosToEdge, class T_RecList, int T_STEP, EngineState T_STATE>
struct EngineEnableIf<false, T_Maze, T_CurrDirPos, T_INIT_DIR, T_DirPosToEdge, T_RecList, T_STEP, T_STATE>
{
  using Type = void;
};

/**
 * Template specialization for the IDLE engine state.
 */
template<class T_Maze, class T_CurrDirPos, Dir T_INIT_DIR, class T_DirPosToEdge, class T_RecList, int T_STEP>
struct Engine<T_Maze, T_CurrDirPos, T_INIT_DIR, T_DirPosToEdge, T_RecList, T_STEP, EngineState::IDLE> :
  Engine<T_Maze, T_CurrDirPos, T_INIT_DIR, T_DirPosToEdge, typename T_RecList::template Append<Record<T_CurrDirPos,
    T_DirPosToEdge, EngineState::IDLE>>, T_STEP + 1, EngineState::FIND_WALL>::Type
{
};

/**
 * Template specialization for the FIND_WALL engine state.
 */
template<class T_Maze, class T_CurrDirPos, Dir T_INIT_DIR, class T_DirPosToEdge, class T_RecList, int T_STEP>
struct Engine<T_Maze, T_CurrDirPos, T_INIT_DIR, T_DirPosToEdge, T_RecList, T_STEP, EngineState::FIND_WALL>
{
  //We append the current state to the list of recorded states.
  using RecList = typename T_RecList::template Append<Record<T_CurrDirPos, T_DirPosToEdge, EngineState::FIND_WALL>>;

  static constexpr bool isWallAhead = IsWallAhead<T_Maze, T_CurrDirPos>::value;
  static constexpr bool isExitFound = IsExitFound<T_Maze, typename T_CurrDirPos::Pos>::value;

  using Type =
    std::conditional_t<isExitFound,
      //If the exit is found, we go into EXIT_FOUND state and the seeking is done.
      Engine<T_Maze, T_CurrDirPos, T_INIT_DIR, T_DirPosToEdge, RecList, T_STEP + 1, EngineState::EXIT_FOUND>,
      //Otherwise, if a wall is in the next cell to the right of the current cell in terms of the current direction,
      //we go into FOLLOW_WALL state.
      std::conditional_t<isWallAhead,
        typename EngineEnableIf<!isExitFound && isWallAhead, T_Maze, typename Rotate<T_CurrDirPos, RotDir::CW>::DirPos,
                                T_INIT_DIR, T_DirPosToEdge, RecList, T_STEP + 1, EngineState::FOLLOW_WALL>::Type,
        //If the condition is false, then we go to the next cell in the current direction keeping the current state.
        typename EngineEnableIf<!isExitFound && !isWallAhead, T_Maze, typename GoAhead<T_CurrDirPos>::DirPos,
                                T_INIT_DIR, T_DirPosToEdge, RecList, T_STEP + 1, EngineState::FIND_WALL>::Type
      >
    >;
};

/**
 * Template specialization for the FOLLOW_WALL engine state.
 * In each step in the FOLLOW_WALL state we check if in front of us there's wall or not. If there's no wall,
 * we assume the wall is to the right of us so we go ahead and turn right to face it, otherwise, we turn left
 * to turn away from the wall, so we can go ahead.
 * For example to follow the wall, basically, the following steps will be performed:
 *   - Assuming that there's a wall in front of use at the initial step, we turn left to turn away from it.
 *   - In the next step we assume we're not in a corner, so there's no wall in front of us again, and we can
 *     go one step forward and turn right to face the wall.
 *   - Now we have already made one step alongside the wall and having the wall in front of us again, we can
 *     start the whole process over again. In case we end up in a corner or a dead-end, we execute the first
 *     step twice or three time respectively.
 */
template<class T_Maze, class T_CurrDirPos, Dir T_INIT_DIR, class T_DirPosToEdge, class T_RecList, int T_STEP>
struct Engine<T_Maze, T_CurrDirPos, T_INIT_DIR, T_DirPosToEdge, T_RecList, T_STEP, EngineState::FOLLOW_WALL>
{
  using RecList = typename T_RecList::template Append<Record<T_CurrDirPos, T_DirPosToEdge, EngineState::FOLLOW_WALL>>;

  //Check if the current position is the saved position which is the closest to the edge of the maze.
  static constexpr bool isCurrPosClosestToEdge  = IsSamePos<T_DirPosToEdge, T_CurrDirPos>::value;
  static constexpr bool isExitFound             = IsExitFound<T_Maze, typename T_CurrDirPos::Pos>::value;
  //Get next valid position and turn either left if there's no wall ahead of right in the other case
  using NextStep      = typename StepOne<T_Maze, T_CurrDirPos>::DirPos;
  using DirPosToEdge  = typename GetClosestDirPosToEdge<T_DirPosToEdge, T_CurrDirPos, T_INIT_DIR>::DirPos;

  using Type =
    std::conditional_t<isExitFound,
      Engine<T_Maze, T_CurrDirPos, T_INIT_DIR, DirPosToEdge, RecList, T_STEP + 1, EngineState::EXIT_FOUND>,
      std::conditional_t<isCurrPosClosestToEdge,
        //If we in the closest position towards the edge of the maze, it means we were following
        //the edge of an "island" or a cycle if we look at the the maze as a graph, so we should
        //leave the "island" by following the direction that we initially chose to find the edge. 
        typename EngineEnableIf<!isExitFound && isCurrPosClosestToEdge, T_Maze, DirPos<typename T_CurrDirPos::Pos, T_INIT_DIR>,
                                T_INIT_DIR, DirPosToEdge, RecList, T_STEP + 1, EngineState::FIND_WALL>::Type,
        //If we're not in the closest position towards the edge, we can continue following the wall
        //using the next valid position.
        typename EngineEnableIf<!isExitFound && !isCurrPosClosestToEdge, T_Maze, NextStep,
                                T_INIT_DIR, DirPosToEdge, RecList, T_STEP + 1, EngineState::FOLLOW_WALL>::Type
      >
    >;
};

/**
 * Template specialization for the EXIT_FOUND engine state.
 */
template<class T_Maze, class T_CurrDirPos, Dir T_INIT_DIR, class T_DirPosToEdge, class T_RecList, int T_STEP>
struct Engine<T_Maze, T_CurrDirPos, T_INIT_DIR, T_DirPosToEdge, T_RecList, T_STEP, EngineState::EXIT_FOUND>
{
  using RecList = typename T_RecList::template Append<Record<T_CurrDirPos, T_DirPosToEdge, EngineState::EXIT_FOUND>>;

  using ExitPos = typename T_CurrDirPos::Pos;
};


// PLAYBACK UTILS

using RecordListPrinterFunc = std::function<void(size_t, int, int, Dir, int, int, Dir, EngineState)>;

/**
 * This struct is used to play back the performed steps using the list of recorded engine states.
 */
template<class T_RecordList, size_t T_COUNT = 0, bool T_CONTINUE = true>
struct RecordListPrinter
{
  static void print(const RecordListPrinterFunc& printer)
  {
    using Record = typename T_RecordList::template GetRecord<T_COUNT>;
    printer(T_COUNT, Record::DirPos::Pos::x, Record::DirPos::Pos::y, Record::DirPos::dir,
        Record::DirPosToEdge::Pos::x, Record::DirPosToEdge::Pos::y, Record::DirPosToEdge::dir, Record::state);
    RecordListPrinter<T_RecordList, T_COUNT + 1, T_COUNT < T_RecordList::size - 1>::print(printer);
  }
};

template<class T_RecordList, size_t T_COUNT>
struct RecordListPrinter<T_RecordList, T_COUNT, false>
{
  static void print(const RecordListPrinterFunc&) {}
};

using MazePrinterFunc = std::function<void(int, int, Dir, int, int, Tile, int, int, bool, bool)>;

template<class T_Maze, int MAX_X = T_Maze::template getSizeOfRow<0>(), int MAX_Y = T_Maze::size, int T_X = 0, int T_Y = 0>
struct MazePrinter
{
  static void print(int x, int y, Dir dir, int xEdge, int yEdge, const MazePrinterFunc& printer)
  {
    printer(x, y, dir, xEdge, yEdge, T_Maze::template get<T_X, T_Y>(), T_X, T_Y, false, false);
    MazePrinter<T_Maze, T_Maze::template getSizeOfRow<T_Y>(), MAX_Y, T_X + 1, T_Y>::print(x, y, dir, xEdge, yEdge, printer);
  }
};

template<class T_Maze, int MAX_X, int MAX_Y, int T_Y>
struct MazePrinter<T_Maze, MAX_X, MAX_Y, MAX_X, T_Y>
{
  static void print(int x, int y, Dir dir, int xEdge, int yEdge, const MazePrinterFunc& printer)
  {
    printer(x, y, dir, xEdge, yEdge, Tile::PATH, 0, 0, true, false);

    constexpr int rowIndex = (T_Y + 1) >= T_Maze::size ? 0 : T_Y + 1;
    MazePrinter<T_Maze, T_Maze::template getSizeOfRow<rowIndex>(), MAX_Y, 0, T_Y + 1>::print(x, y, dir, xEdge, yEdge, printer);
  }
};

template<class T_Maze, int MAX_X, int MAX_Y, int T_X>
struct MazePrinter<T_Maze, MAX_X, MAX_Y, T_X, MAX_Y>
{
  static void print(int x, int y, Dir dir, int xEdge, int yEdge, const MazePrinterFunc& printer)
  {
    printer(x, y, dir, xEdge, yEdge, Tile::PATH, 0, 0, false, true);
  }
};

} //namespace maze_solver
