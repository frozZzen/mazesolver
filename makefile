IDIR=.
CC=g++
CFLAGS=-I$(IDIR) -O3 -static -std=c++14

all:
	$(CC) -o maze_solver $(CFLAGS) *.cpp

.PHONY: clean

clean:
	rm -f maze_solver
