#pragma once

#include "maze.hpp"

#include <iostream>
#include <thread>
#include <array>
#include <cstdint>
#include <cassert>
#include <map>


namespace maze_solver
{


static void test_maze()
{
  using M = Maze<
    //  0  1  2  3  4  5  6  7
    Row<1, 1, 1, 1, 1, 1, 1, 1>, //0 ########
    Row<1, 0, 1, 0, 0, 1, 0, 1>, //1 #@#  # #
    Row<1, 0, 1, 1, 0, 1, 0, 1>, //2 # ## # #
    Row<1, 0, 0, 0, 0, 1, 0, 1>, //3 #    # #
    Row<1, 1, 1, 1, 0, 0, 0, 1>, //4 ####   #
    Row<1, 0, 0, 1, 1, 1, 0, 1>, //5 #  ### #
    Row<1, 1, 0, 0, 0, 0, 0, 1>, //6 ##     #
    Row<1, 1, 1, 1, 0, 1, 1, 1>, //7 #### ###
    Row<1, 0, 0, 0, 0, 0, 0, 2>, //8 #      X
    Row<1, 1, 1, 1, 1, 1, 1, 1>  //9 ########
  >;

  using RunEngine = Engine<M, DirPos<Position<1, 1>, Dir::DOWN>>;
  static_assert(RunEngine::ExitPos::x == 7, "");
  static_assert(RunEngine::ExitPos::y == 8, "");

  static_assert(M::get<4, 5>() == Tile::WALL, "");
  static_assert(M::get<4, 4>() == Tile::PATH, "");
  static_assert(M::get<7, 8>() == Tile::EXIT, "");

  static_assert(Rotate<DirPos<Position<0, 0>, Dir::DOWN>, RotDir::CCW>::DirPos::dir == Dir::RIGHT, "");
  static_assert(Rotate<DirPos<Position<0, 0>, Dir::RIGHT>, RotDir::CCW>::DirPos::dir == Dir::UP, "");
  static_assert(Rotate<DirPos<Position<0, 0>, Dir::UP>, RotDir::CCW>::DirPos::dir == Dir::LEFT, "");
  static_assert(Rotate<DirPos<Position<0, 0>, Dir::LEFT>, RotDir::CCW>::DirPos::dir == Dir::DOWN, "");

  static_assert(Rotate<DirPos<Position<0, 0>, Dir::DOWN>, RotDir::CW>::DirPos::dir == Dir::LEFT, "");
  static_assert(Rotate<DirPos<Position<0, 0>, Dir::LEFT>, RotDir::CW>::DirPos::dir == Dir::UP, "");
  static_assert(Rotate<DirPos<Position<0, 0>, Dir::UP>, RotDir::CW>::DirPos::dir == Dir::RIGHT, "");
  static_assert(Rotate<DirPos<Position<0, 0>, Dir::RIGHT>, RotDir::CW>::DirPos::dir == Dir::DOWN, "");

  static_assert(IsWallAhead<M, DirPos<Position<0, 0>, Dir::DOWN>>::value == true, "");
  static_assert(IsWallAhead<M, DirPos<Position<0, 0>, Dir::RIGHT>>::value == true, "");
  static_assert(IsWallAhead<M, DirPos<Position<7, 9>, Dir::LEFT>>::value == true, "");
  static_assert(IsWallAhead<M, DirPos<Position<7, 9>, Dir::UP>>::value == false, "");
  static_assert(IsWallAhead<M, DirPos<Position<4, 3>, Dir::DOWN>>::value == false, "");
  static_assert(IsWallAhead<M, DirPos<Position<5, 4>, Dir::RIGHT>>::value == false, "");
  static_assert(IsWallAhead<M, DirPos<Position<4, 7>, Dir::LEFT>>::value == true, "");
  static_assert(IsWallAhead<M, DirPos<Position<2, 3>, Dir::UP>>::value == true, "");

  static_assert(IsExitFound<M, Position<7, 8>>::value == true, "");

  static_assert(IsSamePos<Position<4, 3>, Position<4, 3>>::value == true, "");
  static_assert(IsSamePos<Position<4, 3>, Position<4, 4>>::value == false, "");
  static_assert(IsSamePos<Position<4, 3>, Position<4, 2>>::value == false, "");
  static_assert(IsSamePos<Position<4, 3>, Position<5, 3>>::value == false, "");
  static_assert(IsSamePos<Position<4, 3>, Position<3, 3>>::value == false, "");

  static_assert(IsSamePos<typename GoAhead<DirPos<Position<0, 0>, Dir::DOWN>>::DirPos::Pos, Position<0, 1>>::value, "");
  static_assert(IsSamePos<typename GoAhead<DirPos<Position<0, 0>, Dir::RIGHT>>::DirPos::Pos, Position<1, 0>>::value, "");
  static_assert(IsSamePos<typename GoAhead<DirPos<Position<4, 5>, Dir::UP>>::DirPos::Pos, Position<4, 4>>::value, "");
  static_assert(IsSamePos<typename GoAhead<DirPos<Position<4, 5>, Dir::LEFT>>::DirPos::Pos, Position<3, 5>>::value, "");

  static_assert(IsSamePos<typename GetClosestDirPosToEdge<DirPos<Position<3, 4>, Dir::LEFT>,
      DirPos<Position<3, 4>, Dir::LEFT>, Dir::DOWN>::DirPos::Pos, Position<3, 4>>::value, "");
  static_assert(IsSamePos<typename GetClosestDirPosToEdge<DirPos<Position<3, 3>, Dir::LEFT>,
      DirPos<Position<3, 4>, Dir::LEFT>, Dir::DOWN>::DirPos::Pos, Position<3, 4>>::value, "");
  static_assert(IsSamePos<typename GetClosestDirPosToEdge<DirPos<Position<3, 5>, Dir::LEFT>,
      DirPos<Position<3, 4>, Dir::LEFT>, Dir::DOWN>::DirPos::Pos, Position<3, 5>>::value, "");
  static_assert(IsSamePos<typename GetClosestDirPosToEdge<DirPos<Position<3, 4>, Dir::LEFT>,
      DirPos<Position<3, 4>, Dir::LEFT>, Dir::RIGHT>::DirPos::Pos, Position<3, 4>>::value, "");
  static_assert(IsSamePos<typename GetClosestDirPosToEdge<DirPos<Position<2, 4>, Dir::LEFT>,
      DirPos<Position<3, 4>, Dir::LEFT>, Dir::RIGHT>::DirPos::Pos, Position<3, 4>>::value, "");
  static_assert(IsSamePos<typename GetClosestDirPosToEdge<DirPos<Position<4, 4>, Dir::LEFT>,
      DirPos<Position<3, 4>, Dir::LEFT>, Dir::RIGHT>::DirPos::Pos, Position<4, 4>>::value, "");

  static_assert(IsSamePos<typename StepOne<M, DirPos<Position<1, 1>, Dir::UP>>::DirPos::Pos, Position<1, 1>>::value &&
      StepOne<M, DirPos<Position<1, 1>, Dir::UP>>::DirPos::dir == Dir::RIGHT, "");
  static_assert(IsSamePos<typename StepOne<M, DirPos<Position<1, 1>, Dir::RIGHT>>::DirPos::Pos, Position<1, 1>>::value &&
      StepOne<M, DirPos<Position<1, 1>, Dir::RIGHT>>::DirPos::dir == Dir::DOWN, "");
  static_assert(IsSamePos<typename StepOne<M, DirPos<Position<1, 1>, Dir::DOWN>>::DirPos::Pos, Position<1, 2>>::value &&
      StepOne<M, DirPos<Position<1, 1>, Dir::DOWN>>::DirPos::dir == Dir::RIGHT, "");
  static_assert(IsSamePos<typename StepOne<M, DirPos<Position<1, 2>, Dir::RIGHT>>::DirPos::Pos, Position<1, 2>>::value &&
      StepOne<M, DirPos<Position<1, 2>, Dir::RIGHT>>::DirPos::dir == Dir::DOWN, "");
  static_assert(IsSamePos<typename StepOne<M, DirPos<Position<1, 2>, Dir::DOWN>>::DirPos::Pos, Position<1, 3>>::value &&
      StepOne<M, DirPos<Position<1, 2>, Dir::DOWN>>::DirPos::dir == Dir::RIGHT, "");
  static_assert(IsSamePos<typename StepOne<M, DirPos<Position<1, 3>, Dir::RIGHT>>::DirPos::Pos, Position<2, 3>>::value &&
      StepOne<M, DirPos<Position<1, 3>, Dir::RIGHT>>::DirPos::dir == Dir::UP, "");
  static_assert(IsSamePos<typename StepOne<M, DirPos<Position<2, 3>, Dir::UP>>::DirPos::Pos, Position<2, 3>>::value &&
      StepOne<M, DirPos<Position<2, 3>, Dir::UP>>::DirPos::dir == Dir::RIGHT, "");

  using M1 = Maze<
    //  0  1  2  3  4  5  6  7       01234567
    Row<1, 1, 1, 1, 1, 1, 1, 1>, //0 ########
    Row<1, 0, 0, 0, 0, 0, 0, 1>, //1 # @    #
    Row<1, 0, 1, 1, 1, 0, 0, 1>, //2 # ###  #
    Row<1, 0, 0, 1, 0, 0, 0, 1>, //3 #  #   #
    Row<1, 0, 0, 0, 0, 0, 1, 1>, //4 #     ##
    Row<1, 0, 0, 0, 0, 0, 0, 1>, //5 #      #
    Row<1, 0, 0, 0, 0, 0, 0, 1>, //6 #      #
    Row<1, 0, 1, 0, 0, 1, 1, 1>, //7 # #  ###
    Row<1, 0, 0, 0, 0, 0, 0, 2>, //8 #      X
    Row<1, 1, 1, 1, 1, 1, 1, 1>  //9 ########
  >;

  using RunEngine1 = Engine<M1, DirPos<Position<2, 1>, Dir::DOWN>>;
  static_assert(RunEngine1::ExitPos::x == 7, "");
  static_assert(RunEngine1::ExitPos::y == 8, "");
}

static void mazePrinter(int x, int y, Dir dir, int x_min, int y_min, Tile tile, int X, int Y, bool isEol, bool isEof)
{
  static std::map<Dir, const char*> dir2Str = {
      {Dir::UP,     "^"},
      {Dir::DOWN,   "v"},
      {Dir::RIGHT,  ">"},
      {Dir::LEFT,   "<"}
  };

  if (isEol || isEof)
  {
    std::cout << std::endl;
    return;
  }
  if (x == X && y == Y)
  {
    std::cout << dir2Str[dir];
  }
  else if (x_min == X && y_min == Y)
  {
    std::cout << ".";
  }
  else
  {
    std::cout << (tile == Tile::PATH ? " " : (tile == Tile::WALL ? "#" : "X"));
  }
};

template<class T_MazePrinter>
static void printer(size_t count, int x, int y, Dir dir, int x_min, int y_min, Dir dir_min, EngineState state)
{
  std::cout << "Step  " << count << ":\t Pos: (" << x << ", " << y << "), " << getDirStr(dir) << "\t LowestPos: (" << x_min << ", " << y_min << "), " <<
      getDirStr(dir_min) << "\t State: " << getEngineStateStr(state) << std::endl;
  T_MazePrinter::print(x, y, dir, x_min, y_min, mazePrinter);
  std::this_thread::sleep_for(std::chrono::milliseconds(120));
};

static void run()
{
  using namespace std;

  test_maze();


  using M = Maze<
    //  0  1  2  3  4  5  6  7  8  9  10             012345678910
    Row<1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1>,       // 0 ###########11
    Row<1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1>,    // 1 #  @  #    #12
    Row<1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1>, // 2 # # #   #   #
    Row<1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1>, // 3 #  # #### # #
    Row<1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1>, // 4 ##   #      #
    Row<1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1>,    // 5 ## #   #####
    Row<1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1>,       // 6 #  ## #   #
    Row<1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1>,       // 7 #     # # #
    Row<1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 2>,       // 8 # ###   # X
    Row<1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1>,       // 9 #   ###  ##
    Row<1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1>,       //10 # #       #
    Row<1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1>        //11 ###########
  >;

  using RunEngine = Engine<M, DirPos<Position<3, 1>, Dir::DOWN>>;
  using MPrinter = MazePrinter<M>;
  RecordListPrinter<RunEngine::RecList>::print(printer<MPrinter>);

  cout << "Exit Found!" << endl << endl;
}

} //namespace maze_solver
