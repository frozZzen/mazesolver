### Compile-Time Maze Solver ###

***
This is a compile-time maze solver, in other words it finds the exit, saves the individual steps
in compile time and finally plays back the steps in run time, it's for entertainment only.

***
Check out the demo binaries:
```
demo.exe     (huge monolitical binary built with cygwin/g++, it solves a more complex maze)  
demo_vs.exe  (built with VS14, VS can only handle a smaller maze)
```
***